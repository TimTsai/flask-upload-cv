from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, Text, Float
from sqlalchemy.orm import relationship
from . import Base


# sqlalchemy orm object
class Job_CV(Base):
    __tablename__ = 'job_cv'
    id = Column('id', Integer, primary_key=True)
    upload_id = Column('upload_id', Integer)
    cvDocument = Column('cvDocument', Text)
    cvContent = Column('cvContent', Text)
    positionClass = Column('positionClass', String)
    recommend = Column('recommend', Text)
    d_score = Column('Dscore', Float)
    i_score = Column('Iscore', Float)
    s_score = Column('Sscore', Float)
    c_score = Column('Cscore', Float)
    education = Column('education', Float)
    skill = Column('skill', Float)
    trait = Column('trait', Float)
    #
    # def __init__(self, name='', gender=False, email='', phones=list(), addresses=list()):
    #     self.name = name
    #     self.gender = gender
    #     self.email = email
    #     self.phones = phones
    #     self.addresses = addresses


# class Phone(Base):
#     __tablename__ = 'phone'
#     id = Column('id', Integer, primary_key=True)
#     user_id = Column('user_id', Integer, ForeignKey('user.id'))
#     number = Column('number', String)
#     remark = Column('remark', String)
#
#     # def __init__(self, user_id, number, remark):
#     #     self.user_id = user_id
#     #     self.number = number
#     #     self.remark = remark
#
#
# class Address(Base):
#     __tablename__ = 'address'
#     id = Column('id', Integer, primary_key=True)
#     user_id = Column('user_id', Integer, ForeignKey('user.id'))
#     address = Column('address', String)
#     remark = Column('remark', String)
#
#     # def __init__(self, user_id, address, remark):
#     #     self.user_id = user_id
#     #     self.address = address
#     #     self.remark = remark
#
#
# # declared output json schema
# from marshmallow import Schema, fields
#
#
# class PhoneSchema(Schema):
#     id = fields.Int()
#     number = fields.Str()
#     remark = fields.Str()
#
#
# class AddressSchema(Schema):
#     id = fields.Int()
#     address = fields.Str()
#     remark = fields.Str()
#
#
# class UserSchema(Schema):
#     id = fields.Int()
#     name = fields.Str()
#     gender = fields.Bool()
#
#     # add relational object list
#     phones = fields.Nested(PhoneSchema, many=True)
#     addresses = fields.Nested(AddressSchema, many=True)
