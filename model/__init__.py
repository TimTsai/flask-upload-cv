from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import config

# create db session
Base = declarative_base()
engine = create_engine(config.db_connection_string)
DBSession = sessionmaker(bind=engine)
