# coding=utf-8
from uuid import uuid4

from flask import Flask, request, redirect, url_for, render_template, current_app
import os
import json
import glob
import threading
import hashlib
import sqlite3
import config
import time
from model import DBSession
from model.job_model import Job_CV



app = Flask(__name__)
db = DBSession()

upload_path = "uploadr/static/uploads/"


def create_db_cursor():
    db = sqlite3.connect(config.sqlite_db_name)
    cursor = db.cursor()
    return db, cursor


def create_upload_table():
    db, cursor = create_db_cursor()
    cmd = """create table upload_file_info(id integer primary key, folder_name varchar(100), old_name varchar(100), new_name varchar(200));"""
    try:
        cursor.execute(cmd)
    except Exception as e:
        print(e)


def insert_upload_info(folder_name, old_filename, new_filename):
    db, cursor = create_db_cursor()
    cmd = "Insert Into upload_file_info (folder_name, old_name, new_name) Values ('{}','{}', '{}');".format(folder_name,
                                                                                                            old_filename,
                                                                                                            new_filename)
    print(cmd)
    try:
        cursor.execute(cmd)
        db.commit()
        db.close()
        return True
    except Exception as e:
        print(e)
        return False


def select_upload_info_by_newname(new_filename):
    db, cursor = create_db_cursor()
    cmd = "select * from upload_file_info where new_name='{}';".format(new_filename)
    id = 0
    old_name = ''
    print(cmd)
    try:
        result = cursor.execute(cmd)
        for row in result:
            id = row[0]
            folder_name = row[1]
            old_name = row[2]
            break
        db.commit()
        db.close()
        return id, folder_name, old_name
    except Exception as e:
        print(e)
        return False


def select_upload_info_by_folderName(folder_name):
    db, cursor = create_db_cursor()
    cmd = "select * from upload_file_info where folder_name='{}';".format(folder_name)
    id = 0
    old_name = ''
    print(cmd)
    try:
        result = cursor.execute(cmd)
        for row in result:
            id = row[0]
            folder_name = row[1]
            old_name = row[2]
            new_name = row[3]
            break
        db.commit()
        db.close()
        return id, folder_name, old_name, new_name
    except Exception as e:
        print(e)
        return False


create_upload_table()


@app.route("/")
def index():
    return render_template("index.html")


def hash_filename_to_md5(filename):
    md5 = hashlib.md5()
    md5.update(filename.encode("utf8"))
    new_name = md5.hexdigest()
    new_name = "{}.pdf".format(new_name)
    return new_name


@app.route("/upload", methods=["POST"])
def upload():
    """Handle the upload of a file."""
    form = request.form

    # Create a unique "session ID" for this particular batch of uploads.
    upload_key = str(uuid4())

    # Is the upload using Ajax, or a direct POST by the form?
    is_ajax = False
    if form.get("__ajax", None) == "true":
        is_ajax = True

    # Target folder for these uploads.
    target = "uploadr/static/uploads/{}".format(upload_key)
    try:
        os.mkdir(target)
    except:
        if is_ajax:
            return ajax_response(False, "Couldn't create upload directory: {}".format(target))
        else:
            return "Couldn't create upload directory: {}".format(target)

    # print("=== Form Data ===")
    # for key, value in list(form.items()):
    #     print(key, "=>", value)

    for upload in request.files.getlist("file"):
        filename = upload.filename.rsplit("/")[0]
        hash_filename = hash_filename_to_md5(filename)
        destination = "/".join([target, hash_filename])
        # print("Accept incoming file:", hash_filename)
        # print("Save it to:", destination)
        utf_filename = filename.encode('utf-8')
        print(utf_filename)
        upload.save(destination)
        insert_upload_info(upload_key, utf_filename, hash_filename)

    if is_ajax:
        return ajax_response(True, upload_key)
    else:
        return redirect(url_for("upload_complete", uuid=upload_key))


@app.route("/files/<uuid>")
def upload_complete(uuid):
    """The location we send them to at the end of the upload."""

    # Get their files.
    root = upload_path + uuid

    if not os.path.isdir(root):
        return "Error: UUID not found!"

    files = []
    for file in glob.glob("{}/*.*".format(root)):
        fname = file.split(os.sep)[-1]
        id, folder_name, old_name = select_upload_info_by_newname(fname)
        file = {
            'file': fname,
            'filename': old_name,
            'upload_id': uuid
        }
        files.append(file)

    # call api
    target_path = current_app.root_path.replace('uploadr', '') + upload_path + files[0]['upload_id'] + '/' + files[0][
        'file']
    thread_analyze_cv = threading.Thread(target=analyze_cv, args=(uuid, target_path))
    thread_analyze_cv.start()

    return render_template("files.html",
                           uuid=uuid,
                           files=files)


@app.route("/cv_result")
def cv_analysis_result():
    upload_id = request.args.get('upload_id')
    return render_template('cv_analysis_result.html', upload_id=upload_id,
                           get_cv_data_interval=config.get_cv_data_interval)


@app.route("/cv_data")
def cv_data():
    upload_id = request.args.get('upload_id')
    id, folder_name, old_name, new_name = select_upload_info_by_folderName(upload_id)

    loop_times = 3
    loop_sleep_interval_second = 5

    for i in range(loop_times):
        job_cv_list = db.query(Job_CV).filter(Job_CV.upload_id == upload_id).all()
        if len(job_cv_list) > 0:
            # recommend_text = """[{'company': '台積電',
            #                    'job': 'AI 工程師'},
            #                   {'company': '台積電',
            #                    'job': '數據分析師'},
            #                   {'company': '明基',
            #                    'job': '數據分析師'},
            #                   {'company': '明基',
            #                    'job': 'src 工程師'},
            #                   {'company': '資策會',
            #                    'job': '軟體工程師'}
            #                ]"""
            recommend_text = job_cv_list[0].recommend
            recommend_text = "{'data':" + recommend_text + "}"
            recommend_text = recommend_text.replace("'", "\"")
            recommend_text = recommend_text.replace("u", "")
            recommend_json = json.loads(recommend_text)
            return_value = {'cv_data': [job_cv_list[0].education,
                                        job_cv_list[0].skill,
                                        job_cv_list[0].trait],
                            'disc_data': [job_cv_list[0].d_score,
                                          job_cv_list[0].i_score,
                                          job_cv_list[0].s_score,
                                          job_cv_list[0].c_score],
                            'recommend': recommend_json['data']}
            return json.dumps(return_value), 200
        else:
            time.sleep(loop_sleep_interval_second)
            # import datetime
            # print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    return json.dumps({'msg': 'Data not ready. Please reload page.'}), 404


def ajax_response(status, msg):
    status_code = "ok" if status else "error"
    return json.dumps(dict(
        status=status_code,
        msg=msg,
    ))


def analyze_cv(upload_code, upload_filepath):
    import subprocess
    import config
    cmd = config.cvRecommend_command + ' ' + upload_code + ' ' + upload_filepath
    # print(cmd)
    cmd = cmd.split()

    cmd1 = subprocess.Popen(['echo', config.sudo_password], stdout=subprocess.PIPE)
    cmd2 = subprocess.Popen(['sudo', '-S'] + cmd, stdin=cmd1.stdout, stdout=subprocess.PIPE)

    # python3_command = " 0000"
    # print('tim test')
    # subprocess.Popen(python3_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    # subprocess.call("%s cvRecommend.py" % (sys.exc shell=True)
