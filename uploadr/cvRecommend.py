import datetime


def print_test():
    now_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    f = open('/home/tim-docker/python_proj/flask-upload-cv/test', 'w+-')
    f.write(now_str)
    print(now_str)


if __name__ == '__main__':
    print_test()
