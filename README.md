1.  安裝 virtualenv 
sudo pip install virtualenv

2. 建立 virtual 環境
virtualenv venv

3. 進入 virtual 環境
. venv/bin/activate

4. 執行網站
python runserver.py